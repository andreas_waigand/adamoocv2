// Karma configuration
// Generated on Wed Apr 15 2015 14:17:57 GMT+0200 (Mitteleuropäische Sommerzeit)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
		'../assets/js/jquery.min.js',
		'../assets/js/jquery.easing.1.3.min.js',
		'../assets/js/jquery.form.js',
		'../assets/js/jquery.validate.min.js',
		'../bower_components/angular/angular.js',
		'../bower_components/angular-route/angular-route.min.js',
		'../bower_components/angular-validation-match/dist/angular-input-match.js',
		'../bower_components/allmighty-autocomplete/script/autocomplete.js',
		'../bower_components/angular-cookie/angular-cookie.min.js',
		'../bower_components/angular-mocks/angular-mocks.js',
		'../assets/js/bootstrap.min.js',
		'../assets/js/bootstrap-hover-dropdown.min.js',
		'../assets/js/skrollr.min.js',
		'../assets/js/skrollr.stylesheets.min.js',
		'../assets/js/waypoints.min.js',
		'../assets/js/waypoints-sticky.min.js',
		'../assets/js/owl.carousel.min.js',
		'../assets/js/jquery.isotope.min.js',
		'../assets/js/jquery.easytabs.min.js',
		'../assets/js/viewport-units-buggyfill.js',
		'../assets/js/scripts.js',
		'../bower_components/pouchdb/dist/pouchdb.js',
		'../bower_components/pouchdb-authentication/dist/pouchdb.authentication.js',
		'../bower_components/marked/lib/marked.js',
		'../bower_components/angular-marked/angular-marked.min.js',
		'../assets/js/factory/PouchDBFactory.js',
		'../assets/js/service/PouchStartupService.js',
		'../assets/js/service/PouchLoginService.js',
		'../assets/js/service/LoginService.js',
		'../assets/js/service/PouchSkillService.js',
		'../assets/js/service/CouchSetupService.js',
		'../assets/js/service/PouchPluginService.js',
		'../assets/js/service/PouchCourseService.js',
		'../assets/js/controller/CourseCtrl.js',
		'../assets/js/controller/CourseEditCtrl.js',
		'../assets/js/controller/LoginCtrl.js',
		'../assets/js/controller/SingleCourseCtrl.js',
		'../assets/js/controller/YourCourseCtrl.js',
		'../assets/js/controller/SkillEditorController.js',
		'../assets/js/controller/SkillCtrl.js',
		'../assets/js/controller/PluginCtrl.js',
		'../assets/js/controller/PluginEditCtrl.js',
		'../assets/js/controller/SetUpCtrl.js',
		'../assets/js/controller/SignUpCtrl.js',
		'../assets/js/controller/FootCtrl.js',
		'../assets/js/controller/NavCtrl.js',
		'../assets/js/controller/LandingCtrl.js',
		'../assets/js/bones.js',
		'tests/*/*.js'
    ],


    // list of files to exclude
    exclude: [
      'no'
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
