describe('pouchDBService.login', function () {


    var loginService;

    var exampleUserDoc = {
        "_id": "userinfo",
        "usertype": "TestType"
    };

    var bobUser = {
        username: "Bob",
        usertype: "TestType"
    };

    var fullfilledLoginPromise;
    var fullfilledGetSession;
    var couchDbUserObject = {
        userCtx: {roles: ["testrole"]}
    };

    var $rootScope;

    beforeEach(module('pouch.login', function ($provide) {

        var self = this;

        mockDependency = function pouchDBFactory() {

            return function (dbName) {
                var spy = {
                    get: function () {
                    }, bulkDocs: function () {
                    }, login: function () {
                    }, getSession: function () {
                    }
                };
                spyOn(spy, 'get').and.returnValue(exampleUserDoc);
                spyOn(spy, 'bulkDocs').and.returnValue(spy);
                spyOn(spy, 'login').and.returnValue(fullfilledLoginPromise);
                spyOn(spy, 'getSession').and.returnValue(fullfilledGetSession);
                return spy;
            };


        }
        $provide.factory('pouchDB', mockDependency);
        $provide.value('pouchDBService.plugins', mockDependency);
    }));


    beforeEach(inject(["pouchDBService.login", "$q", "$rootScope", function (login, $q, rootScope) {
        var loginDef = $q.defer();
        loginDef.resolve({});
        fullfilledLoginPromise = loginDef.promise;

        var sessionDef = $q.defer();
        sessionDef.resolve(couchDbUserObject);
        fullfilledGetSession = sessionDef.promise;

        $rootScope = rootScope;
        loginService = login;
    }]));


    describe('pouchDBService.login', function () {
        it('returns additional userdata', function () {
            expect(loginService.getAdditionalUserData("bob")).toEqual(exampleUserDoc);
        });
        it('Check if user Document is created correctly', function () {
            expect(loginService.createBasicDocs(bobUser).bulkDocs.calls.argsFor(0)[0][3]).toEqual(exampleUserDoc);
        });
        it('Login to remote DB with valid credentials', function () {
            var result;

            loginService.loginToRemoteDb("good", "good").then(function (resultFromService) {
                result = resultFromService;
            });
            $rootScope.$apply();
            expect(result).toEqual({roles: ['testrole']});
        });
    });
});