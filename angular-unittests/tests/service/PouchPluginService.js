describe('pouchDBService.plugins', function () {


    var pluginService
    var $q;

    var exampleUserDoc = {
        "_id": "userinfo",
        "usertype": "TestType"
    };

    var bobUser = {
        username: "Bob",
        usertype: "TestType"
    };

    var pluginCollectionPromise;
    var fullfilledGetSession;
    var couchDbUserObject = {
        userCtx: {roles: ["testrole"]}
    };

    var $rootScope;
    var globalSpy;
    var testPlugin= {name: 'testplugin', description:'testdescription', editURL:"http://example.org", createURL: "http://example.org"}


    beforeEach(module('pouch.mooc', function ($provide) {

        var self = this;

        mockDependency = function pouchDBFactory() {

            return function (dbName) {

                    globalSpy = {
                        get: function () {
                        }, bulkDocs: function () {
                        }, login: function () {
                        }, getSession: function () {
                        }, query: function() {
                        }, post: function(params) {
                        }
                    };
                    spyOn(globalSpy, 'get').and.returnValue(exampleUserDoc);
                    spyOn(globalSpy, 'bulkDocs').and.returnValue(globalSpy);
                    spyOn(globalSpy, 'getSession').and.returnValue(fullfilledGetSession);
                    spyOn(globalSpy, 'query').and.returnValue({ then: function(callback){
                        callback({rows: [{value:{name:"testplugin", createURL: "http://example.org"}},{value:{name:"testpluginnoCreate"}}]});
                    }});
                    spyOn(globalSpy, 'post').and.returnValue({ then: function(callback){
                        callback();
                        return globalSpy;
                    }});

                    return globalSpy;
            };


        };
        $provide.factory('pouchDB', mockDependency);
    }));


    beforeEach(inject(["pouchDBService.plugins", "$q", "$rootScope", function (plugins, $q, rootScope)  {

        var sessionDef = $q.defer();
        sessionDef.resolve(couchDbUserObject);
        fullfilledGetSession = sessionDef.promise;

        $rootScope = rootScope;
        pluginService = plugins;
    }]));



    describe('pouchDBService.login', function () {
        it('getAllPlugins returns all Plugins', function () {
            var result;

            pluginService.getAllPlugins(false).then(function (resultFromService) {
                result = resultFromService;
            });
            $rootScope.$apply();
            expect(result).toEqual({ testplugin: Object({ name: 'testplugin', createURL: 'http://example.org' }), set: true, testpluginnoCreate: Object({ name: 'testpluginnoCreate' }) });
        });
        it('getAllPluginsWithCreateUrls returns all Plugins with Create Urls', function () {
            var result;

            pluginService.getAllPluginsWithCreateURLs(false).then(function (resultFromService) {
                result = resultFromService;
            });
            $rootScope.$apply();
            expect(result).toEqual([ Object({ name: 'testplugin', createURL: 'http://example.org' }) ]);
        });
    });
});