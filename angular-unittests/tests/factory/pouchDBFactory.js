describe('userNameConverter', function () {
    beforeEach(module('pouch.base'));

    var $factory;

    beforeEach(inject(function (_userNameConverter_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        userNameConverter = _userNameConverter_;
    }));

    describe('userNameConverter', function () {
        it('returns userdb-0062006f0062 if bob is given', function () {
            expect(userNameConverter("bob")).toEqual('userdb-0062006f0062');
        });
        it('returns userdb- if nothing is given', function () {
            expect(userNameConverter("")).toEqual('userdb-');
        });
        it('returns userdb-59298b74 if 天譴 is given', function () {
            expect(userNameConverter("天譴")).toEqual('userdb-59298b74');
        });
    });
});