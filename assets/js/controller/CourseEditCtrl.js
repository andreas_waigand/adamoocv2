(function(){
    var app = angular.module("courseEditCtrl", ["pouch.mooc", "autocomplete"]);

    app.controller("CourseEditorController",['$rootScope','$scope', "pouchDBService.skills","$routeParams", "pouchDBService.courses", function($rootScope, $scope, skills, $routeParams, courses){

        function initCourse(){
            $scope.currentReqs = [];
            $scope.currentSkill = "";
        }



        skills.getAllSkills().then(function(skillsList){

            $scope.skills = skillsList;
            $scope.currentReqs = [];

            if($routeParams.courseID){
                courses.getSingleCourse($routeParams.courseID).then(function(course){
                    for(var i = 0; i < course.skills.length; i++){
                        course.skills[i].skill = $scope.skills.skillById[course.skills[i]._id];
                        var requirements = [];
                        for(var j = 0; j < course.skills[i].requirements.length; j++){
                            requirements.push({_id: course.skills[i].requirements[j]})
                        }
                        course.skills[i].requirements = requirements;
                    }
                    if(course.visbility == "public")
                        $scope.visibility = "Public (Anyone can join)";
                    else
                        $scope.visibility = "Private (An admin has to add students)";
                    $scope.course = course;
                });
            }else{
                $scope.course = {};
            }

        });

        $scope.addRequirement = function(){
            $scope.currentReqs.push({idstring: ""});
        }


        $scope.add = function(){
            if($scope.currentSkill && $scope.skills.skillMap[$scope.currentSkill]){
                $scope.course.skills = $scope.course.skills || [];
                var requirements = $scope.currentReqs;
                var skill = { skill: $scope.skills.skillMap[$scope.currentSkill],
                              requirements: []};

                for(var i = 0; i < requirements.length; i++){
                    skill.requirements.push($scope.skills.skillMap[requirements[i].idstring]);
                }

                $scope.course.skills.push(skill);
                initCourse();
            }
        }

        $scope.create = function(){
            $scope.busy = true;
            if($scope.visibility.substring(0,3) == "Pub"){
                    $scope.course.visibility = "public";
            }
            else{
                $scope.course.visibility = "private";
            }
            if($scope.course._id && $scope.course._rev)
                courses.updateCourse($scope.course).then(function(result){
                    $scope.success = true;
                    $scope.course._rev = result._rev;
                    $scope.busy = false;
                    $scope.$apply();
                });
            else{
                courses.submitCourse($scope.course).then(function(result){
                    $scope.course._id = result.id;
                    $scope.course._rev = result.rev;
                    $scope.success = true;
                    $scope.busy = false;
                    $scope.$apply();
                });

            }
        }

        angular.element(document).ready(function () {


            $('.icon-overlay a').prepend('<span class="icn-more"></span>');


            //allInOne();
        });



    }]);


})();