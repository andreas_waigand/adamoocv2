(function(){
   var app = angular.module("navCtrl", ['login']);

    app.controller("NavController",['$rootScope','$scope', 'loginService','UserData', function($rootScope, $scope, login, userdata){

        $scope.loggedIn = login;
        $scope.userdata = userdata;

        $scope.logout = function(){
            login.logout($rootScope.user);
        };


    }]);
    app.directive("tsuNavigation", function(){
       return {
           templateUrl: "assets/snippets/navUI.html",
           controller: "NavController"
       };
    });

})();
