(function(){

    var app = angular.module("setupCtrl", ['login', 'pouch.startup', 'couchSetup']);



    app.controller("SetupController", ['$scope','UserData', 'loginService','pouchDBService', '$rootScope', '$location','couchSetupService','pouchDBService.login', function ($scope, userData, loginService, pouch, $rootScope, $location, couchSetup) {
        $scope.user = {};

        $scope.displayLoginError = function () {
            $scope.loginError = true;
        };

        $scope.login = function () {
            $scope.isBusy = true;
            loginService.login($scope.user.username, $scope.user.password, $scope.user.remember).then(function (result) {
                if (result.valid == "valid") {
                couchSetup.setupCouchDb($scope.user.username).then(function(){
                    $scope.isBusy = false;
                    $scope.success = true;
                });
                } else {
                    console.log(result.online + " " + result.valid);
                    $scope.isBusy=false;
                    $scope.displayLoginError();
                }

            });

        };


    }]);


})();


