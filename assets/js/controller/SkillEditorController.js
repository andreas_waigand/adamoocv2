(function(){
    var app = angular.module("yourCourseCtrl");

    app.controller("SkillEditorController",['$rootScope','$scope', "pouchDBService.plugins", "UserData", function($rootScope, $scope, plugins,user){

        plugins.getAllPluginsWithCreateURLs().then(function(plugins){
           $scope.plugins = plugins;

        });


        angular.element(document).ready(function () {

            $('.icon-overlay a').prepend('<span class="icn-more"></span>');

        });



    }]);


})();