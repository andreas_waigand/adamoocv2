(function(){
    var app = angular.module("courseCtrl", ["pouch.mooc", "login"]);

    app.controller("CourseController",['$rootScope','$scope', "pouchDBService.courses", "UserData", function($rootScope, $scope, courses, user){

        courses.getAllCourses(!user.editCourses).then(function(result){
           $scope.courses = result;
           $scope.userdata = user;
        });


        $scope.deleteCourse = function(courseid, revid){
            courses.deleteCourse(courseid, revid).then(function(result){
                for(var i = 0; i < $scope.courses.length; i++){
                    if($scope.courses[i]._id == courseid){
                        $scope.courses.splice(i,1);
                    }
                }
                $scope.$apply();
            });
        }

        angular.element(document).ready(function () {

            $('.icon-overlay a').prepend('<span class="icn-more"></span>');


            //allInOne();
        });



    }]);


})();