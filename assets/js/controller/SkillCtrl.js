(function(){
    var app = angular.module("skillListCtrl", ["pouch.mooc", "login"]);

    app.controller("SkillListController",['$rootScope','$scope', "pouchDBService.skills", "UserData", function($rootScope, $scope, skills, user){

        skills.getAllSkillsFromServer().then(function(result){
           $scope.skills = result;
           $scope.userdata = user;
        });


        $scope.deleteSkill = function(skillid, revid){
            skills.deleteGeneralDoc(skillid, revid).then(function(result){
                for(var i = 0; i < $scope.skills.length; i++){
                    if($scope.skills[i]._id == skillid){
                        $scope.skills.splice(i,1);
                    }
                }
                $scope.$apply();
            });
        }

        angular.element(document).ready(function () {

            $('.icon-overlay a').prepend('<span class="icn-more"></span>');


            //allInOne();
        });



    }]);


})();