(function(){

    var app = angular.module("loginCtrl", ['login', 'pouch.startup']);



    app.controller("LoginController", ['$scope','UserData', 'loginService','pouchDBService', '$rootScope', '$location','ipCookie', function ($scope, userData, loginService, pouch, $rootScope, $location) {
        $scope.user = {};
        $scope.user.remember = true;

        $scope.displayLoginError = function () {
            $scope.loginError = true;
        };

        $scope.login = function () {
            $scope.isBusy = true;
            loginService.login($scope.user.username, $scope.user.password, $scope.user.remember).then(function (result) {
                if (result.valid == "valid") {
                    $rootScope.user = $scope.user.username;
                    userData.name = $scope.user.username;
                    var startupProm = pouch.generalStartup(result.online == "online", $scope.user.username);
                    startupProm.then(function(res){
                        loginService.getAdditionalUserData(userData.name).then(function(userinfo){
                            userData.usertype = userinfo.usertype;
                            userData.loggedIn = true;
                            loginService.loggedIn = true;
                            $scope.isBusy=false;
                            $rootScope.$apply(function() {
                                if(loginService.plannedRoute)
                                    $location.path(loginService.plannedRoute);
                                else{
                                    $location.path("/");
                                }
                            });
                        });
                    });
                } else {
                    console.log(result.online + " " + result.valid);
                    $scope.isBusy=false;
                    $scope.displayLoginError();
                }

            });

        };


    }]);

    app.directive("busyCog", ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            template: '<i class="icn icon-cog icon-spin ng-hide" ng-show="show"></i>',
            scope: {
                show: '='
            }
        };
    }]);

})();


