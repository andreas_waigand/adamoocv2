(function(){
   var app = angular.module("footCtrl", []);

    app.controller("FooterController",['$rootScope','$scope', function($rootScope, $scope){

        angular.element(document).ready(function () {

            allInOne();

        });

    }]);
    app.directive("tsuFooter", function(){
       return {
           templateUrl: "assets/snippets/footerUI.html",
           controller: "FooterController"
       };
    });

})();
