(function(){

    var app = angular.module("signUpCtrl", ['login', 'pouch.startup', 'validation.match']);

    app.controller("SignUpController", ['$scope', 'loginService','pouchDBService', '$rootScope', '$location', 'UserData', function ($scope, loginService, pouch, $rootScope, $location, userData) {
        $scope.user = {};

        $scope.displayLoginError = function () {
            $scope.loginError = true;
        };

        $scope.options = [
            "Participant",
            "Avoidant",
            "Collaborative",
            "Competitive",
            "Dependent",
            "Independent"
        ];


        $scope.submit = function () {
            $scope.isBusy = true;
            loginService.signUp($scope.user).then(function(result){
                $rootScope.user = $scope.user.username;
                userData.name = $scope.user.username;
                userData.loggedIn = true;
                var startupProm = pouch.generalStartup(result.online == "online", $scope.user.username);
                startupProm.then(function(res){
                    loginService.getAdditionalUserData(userData.name).then(function(userinfo){
                        userData.usertype = userinfo.usertype;
                        userData.loggedIn = true;
                        loginService.loggedIn = true;
                        $scope.isBusy=false;
                        $rootScope.$apply(function() {
                            if(loginService.plannedRoute)
                                $location.path(loginService.plannedRoute);
                            else{
                                $location.path("/");
                            }
                        });

                    });

                });
            }).catch(function(error){
                $scope.displayLoginError();
            })
        };


    }])

})();


