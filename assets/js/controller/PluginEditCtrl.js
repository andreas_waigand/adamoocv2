(function(){
    var app = angular.module("pluginEditCtrl", ["pouch.mooc"]);

    app.controller("PluginEditorController",['$rootScope','$scope', "pouchDBService.plugins","$routeParams", function($rootScope, $scope, plugins, $routeParams){

        if($routeParams.pluginID){
            $scope.busy = true;
            plugins.getSinglePlugin($routeParams.pluginID).then(function(pluginDoc){
                $scope.plugin = pluginDoc;
                $scope.busy = false;
                $scope.$apply();
            });
        }

        $scope.create = function(){
            $scope.busy = true;
                plugins.submitPlugin($scope.plugin).then(function(result){
                    $scope.plugin._id = result.id;
                    $scope.plugin._rev = result.rev;
                    $scope.success = true;
                    $scope.busy = false;
                    $scope.$apply();
                });
        };

        angular.element(document).ready(function () {

            $('.icon-overlay a').prepend('<span class="icn-more"></span>');

        });



    }]);


})();