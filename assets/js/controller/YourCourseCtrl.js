(function(){
    var app = angular.module("yourCourseCtrl", ["pouch.mooc", "login"]);

    app.controller("YourCourseController",['$rootScope','$scope', "pouchDBService.skills", "UserData","pouchDBService.courses", function($rootScope, $scope, skills,user,courses){

        courses.getAllCoursesForUser($rootScope.user).then(function(result){
           $scope.courses = result;
            $scope.userdata = user;
        });

        $scope.deleteCourse = function(courseid, revid){
            courses.deleteCourse(courseid, revid).then(function(result){
                for(var i = 0; i < $scope.courses.length; i++){
                    if($scope.courses[i]._id == courseid){
                        $scope.courses.splice(i,1);
                    }
                }
                $scope.$apply();
            });
        }

        angular.element(document).ready(function () {


            $('.icon-overlay a').prepend('<span class="icn-more"></span>');


            //allInOne();
        });



    }]);


})();