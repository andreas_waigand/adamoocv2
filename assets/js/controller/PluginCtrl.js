(function(){
    var app = angular.module("pluginListCtrl", ["pouch.mooc", "login"]);

    app.controller("PluginListController",['$rootScope','$scope', "pouchDBService.plugins", "UserData", function($rootScope, $scope, plugins, user){

        plugins.getAllPlugins().then(function(plugins){
            $scope.plugins = plugins;
            delete $scope.plugins.set;
            $scope.userdata = user;
        });

        $scope.deletePlugin = function(pluginid, revid){
            plugins.deleteGeneralDoc(pluginid, revid).then(function(result){
                for(var plugin in $scope.plugins){
                    if($scope.plugins[plugin]._id && $scope.plugins[plugin]._id == pluginid){
                        delete $scope.plugins[plugin];
                    }
                }
                $scope.$apply();
            });
        };


        angular.element(document).ready(function () {

            $('.icon-overlay a').prepend('<span class="icn-more"></span>');


            //allInOne();
        });



    }]);


})();