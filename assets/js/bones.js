(function(){
    var app = angular.module('adamoocApp', ['ngRoute','hc.marked','pluginEditCtrl', 'navCtrl', 'pluginListCtrl','skillListCtrl', 'landCtrl', 'footCtrl', 'loginCtrl', 'login', 'signUpCtrl', 'courseCtrl', 'courseEditCtrl', 'yourCourseCtrl', 'singleCourseCtrl', 'setupCtrl', 'couchSetup', 'ipCookie']);

    app.run(['loginService','ipCookie', "$rootScope", "UserData", function (login, cookie, $rootScope, userData) {
        if(cookie("username")){
            $rootScope.user = cookie("username").name;
            userData.name = cookie("username").name;
            userData.loggedIn = true;
            login.loggedIn = true;
        }
        else{
            login.loggedIn = false;
            login.startListeningToRouteChance();
        }
    }]);

    app.config(['$routeProvider' ,function ($routeProvider) {

        function StartUpCheck(pouchDBService,$rootScope,login,$q, userData){
            var def = $q.defer();
            if(userData.name){
                login.checkIfLoggedIn().then(function(result){
                    userData.name = userData.name || result.name;
                    pouchDBService.generalStartup(result.online, result.name || $rootScope.user).then(function(){
                        login.getAdditionalUserData(userData.name).then(function(result){
                            userData.usertype = result.usertype;
                        });
                        def.resolve();
                    });
                });
            }else{
                def.resolve();
            }
            return def.promise;
        }

        $routeProvider
            .when('/', {
                templateUrl: 'assets/snippets/landingUI.html',
                controller: 'LandingController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/setup', {
                templateUrl: 'assets/snippets/setupUI.html',
                controller: 'SetupController'
            }).when('/login', {
                templateUrl: 'assets/snippets/loginUI.html',
                controller: 'LoginController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/signup', {
                templateUrl: 'assets/snippets/signupUI.html',
                controller: 'SignUpController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/course', {
                templateUrl: 'assets/snippets/courseUI.html',
                controller: 'CourseController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/course/:courseID', {
                templateUrl: 'assets/snippets/singleCourseUI.html',
                controller: 'SingleCourseController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData',"pouchDBService.skills","$routeParams","pouchDBService.courses", function (pouchDBService,$rootScope,login,$q, userData,skills, $routeParams, courses) {
                        var def = $q.defer();
                        StartUpCheck(pouchDBService,$rootScope,login,$q, userData).then(function(){
                            courses.updateEnrolledStatusInUserData().then(function(){
                                if(userData.enrolledIn[$routeParams.courseID]){
                                    skills.updateOpenSkillsForUserInCourse(userData.name,$routeParams.courseID).then(function(){
                                        def.resolve();
                                    });
                                }else{
                                    def.resolve();
                                }
                            });
                        });
                        return def.promise;
                    }]
                }
            }).when('/yourcourse', {
                templateUrl: 'assets/snippets/yourCourseUI.html',
                controller: 'YourCourseController',
                controllerAs: 'skillCtrl',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/skills/editor', {
                templateUrl: 'assets/snippets/skillsUI.html',
                controller: 'SkillEditorController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/skills', {
                templateUrl: 'assets/snippets/skillList.html',
                controller: 'SkillListController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/plugins', {
                templateUrl: 'assets/snippets/pluginList.html',
                controller: 'PluginListController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/plugins/editor/:pluginID?', {
                templateUrl: 'assets/snippets/singlePluginEditor.html',
                controller: 'PluginEditorController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            }).when('/courseeditor/:courseID?', {
                templateUrl: 'assets/snippets/singleCourseEditorUI.html',
                controller: 'CourseEditorController',
                resolve: {
                    pouch: ["pouchDBService",'$rootScope','loginService','$q','UserData', function (pouchDBService,$rootScope,login,$q, userData) {
                        return StartUpCheck(pouchDBService,$rootScope,login,$q, userData);
                    }]
                }
            })
            .otherwise({
                redirectTo: '/'
            });
  /*          .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl'
            })
            .when('/skilltree', {
                templateUrl: 'views/skilltree.html',
                controller: 'SkilltreeCtrl'
            }).when('/login', {
                templateUrl: 'views/loginUI.html',
                controller: 'LoginController',
                controllerAs: 'loginCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });*/
    }]);
})();