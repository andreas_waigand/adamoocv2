(function(){
    var app = angular.module("pouch.mooc");

    app.service("pouchDBService.courses", ['$q','pouch.config','$rootScope', '$http', "pouchDBService.skills.config", "pouchDB", "Plugins", "UserData","userNameConverter", "pouchDBService.plugins" ,function($q,mainconfig, $rootScope, $http, config, pouchDBFactory, pluginData, userData,userNameConverter, plugins){

        function deleteFromGeneralDb(docid, revid){
            var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
            return generalDb.remove(docid,revid);
        }


        function disenrollFromCourse(courseid){
            userData.enrolledIn[courseid] = false;
        }

        function enrollInCourse(courseid){
            userData.enrolledIn[courseid] = true;
        }

        //Private

        function removeAndDisenrollCourseIfNoLongerExists(idsOfCourses, CourseDocuments, User){
            var removeAndDisenroll = [];
            for(var i = 0; i < idsOfCourses.length; i++){
                if(!CourseDocuments[i]){
                    removeAndDisenroll.push(idsOfCourses[i]);
                }
            }
            CourseDocuments = CourseDocuments.filter(function(n){ return n != undefined });
            if(removeAndDisenroll.length > 0){
                pouchSkills.DisenrollFromCourse(User, removeAndDisenroll);
            }

            return CourseDocuments;
        }



        function addHrefsToCourseDocuments(coursedocs){
            for(var i = 0; i < coursedocs.length; i++){
                coursedocs[i].href = "#/course/"+coursedocs[i]._id;
            }
            return coursedocs;
        }

        function addSkillsToCourseDocument(coursedoc){
            var def = $q.defer();
            var skillIds = [];
            var reqsBySkill = {};
            for(var i = 0; i < coursedoc.skills.length; i++){
                skillIds.push(coursedoc.skills[i].id);
                reqsBySkill[coursedoc.skills[i].id] = coursedoc.skills[i].requirements;

            }
            getAllDocumentsOfIdsInGeneralDb(skillIds).then(function(result){
                plugins.getAllPlugins().then(function(){
                    coursedoc.skills = result;
                    for(var i = 0; i < coursedoc.skills.length; i++){
                        var responsiblePlugin = pluginData[coursedoc.skills[i].responsible];
                        if(responsiblePlugin.singleSkill)
                            coursedoc.skills[i].href = responsiblePlugin.skillURL;
                        else{
                            coursedoc.skills[i].href = responsiblePlugin.skillURL + coursedoc.skills[i]._id;
                            coursedoc.skills[i].edithref = responsiblePlugin.editURL + coursedoc.skills[i]._id;
                        }
                        coursedoc.skills[i].requirements = reqsBySkill[coursedoc.skills[i]._id];
                    }
                    def.resolve(coursedoc);
                })

            });
            return def.promise;
        }

        function getAllCourseIdsInWhichUserIsEnrolled(user){
            var def = $q.defer();
            var userDb = pouchDBFactory(userNameConverter(user));
            userDb.get(config.enrolledDocument).then(function(result){
                def.resolve(result.enrolledIn);
            });
            return def.promise;
        }

        function getAllDocumentsOfIdsInGeneralDb(ids){
            var def = $q.defer();
            var generalDb = pouchDBFactory(config.generalDB);
            generalDb.allDocs({include_docs:true, keys:ids}).then(function(result){
                var docs = [];
                for(var i = 0; i < result.rows.length; i++){
                    docs.push(result.rows[i].doc);
                }
                def.resolve(docs);
            });

            return def.promise;
        }



        var pouchSkills = {




            /**
             * Submits a course to the general db.
             * This is only possible when you are admin and online.
             */


            updateCourse:function(course){
                var def = $q.defer();
                var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
                var courseDoc = {
                    title: course.title,
                    _id: course._id,
                    _rev: course._rev,
                    type: config.courseType,
                    img: course.img,
                    furtherReading: course.furtherReading,
                    description: course.description,
                    visbility: course.visibility,
                    skills: []
                }
                for(var i = 0; i < course.skills.length; i++){
                    var singleSkill = {
                        id: course.skills[i].skill._id,
                        requirements: []
                    }
                    for(var j = 0; j < course.skills[i].requirements.length; j++){
                        singleSkill.requirements.push(course.skills[i].requirements[j]._id);
                    }
                    courseDoc.skills.push(singleSkill);
                }

                return generalDb.put(courseDoc);
            },



            enrollInCourse:function(user,course){
                var def = $q.defer();
                var userDb = pouchDBFactory(userNameConverter(user));
                userDb.get(config.enrolledDocument).then(function(doc){
                    doc.enrolledIn.push(course);
                    userDb.put(doc).then(function(result){
                        enrollInCourse(course);
                        def.resolve(doc);
                    })
                })
                return def.promise;
            },

            /**
             *
             * @param user
             * @param courses Array(!) of courseids to disenroll from
             * @returns {r.promise|promise|.QueueItem.promise|.TaskQueue.promise|TaskQueue.promise|require.QueueItem.promise|*}
             * @constructor
             */

            DisenrollFromCourse: function(user, courses){
                var def = $q.defer();
                var userDb = pouchDBFactory(userNameConverter(user));
                userDb.get(config.enrolledDocument).then(function(doc){
                    var enrolledIn = doc.enrolledIn;
                    for(var i = 0; i < courses.length; i++){
                        disenrollFromCourse(courses[i]);
                        while(enrolledIn.indexOf(courses[i])> -1){
                            var index = enrolledIn.indexOf(courses[i]);
                            enrolledIn.splice(index,1);
                        }
                    }
                    doc.enrolledIn = enrolledIn;
                    userDb.put(doc).then(function(result){
                        def.resolve(doc); });
                    });
                return def.promise;
            },

            submitCourse:function(course){
             var def = $q.defer();
             var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
             var courseDoc = {
                 title: course.title,
                 type: config.courseType,
                 description: course.description,
                 furtherReading: course.furtherReading,
                 img: course.img,
                 visbility: course.visibility,
                 skills: []
             };
             for(var i = 0; i < course.skills.length; i++){
                 var singleSkill = {
                     id: course.skills[i].skill._id,
                     requirements: []
                 };
                 for(var j = 0; j < course.skills[i].requirements.length; j++){
                     if(course.skills[i].requirements[j] && course.skills[i].requirements[j]._id)
                        singleSkill.requirements.push(course.skills[i].requirements[j]._id);
                 }
                 courseDoc.skills.push(singleSkill);
             }

             return generalDb.post(courseDoc);
            },


            getSingleCourse: function(courseID){
                var def = $q.defer();
                getAllDocumentsOfIdsInGeneralDb([courseID]).then(function(result){
                    addHrefsToCourseDocuments(result);
                    var courseDoc = result[0];
                    addSkillsToCourseDocument(courseDoc).then(function(expandedCourseDoc){
                        def.resolve(expandedCourseDoc);
                    })
                });

                return def.promise;

            },

            deleteCourse:function(courseid, revid){
                return deleteFromGeneralDb(courseid,revid);
            },

            updateEnrolledStatusInUserData: function(){
                var def = $q.defer();
                getAllCourseIdsInWhichUserIsEnrolled(userData.name).then(function(idOfCourses){
                    userData.enrolledIn = {};
                    for(var i = 0; i < idOfCourses.length; i++){
                        userData.enrolledIn[idOfCourses[i]] = true;
                    }
                    def.resolve();
                });

                return def.promise;
            },


            getAllCoursesForUser: function(user){
                var def = $q.defer();
                getAllCourseIdsInWhichUserIsEnrolled(user).then(function(idsOfCourses){
                        var IdsOfCourses = idsOfCourses;
                    getAllDocumentsOfIdsInGeneralDb(idsOfCourses).then(function(courseDocuments){
                        courseDocuments = removeAndDisenrollCourseIfNoLongerExists(idsOfCourses, courseDocuments, user);
                        addHrefsToCourseDocuments(courseDocuments);
                        def.resolve(courseDocuments);
                    })}
                );
                return def.promise;
            },

            getAllCourses: function(publicOnly){
                var def = $q.defer();
                var generalDb = pouchDBFactory(config.generalDB);
                generalDb.query(config.coursecollection).then(function(courses){
                    var returnList = [];
                    for(var i = 0; i < courses.rows.length; i++){
                        if(publicOnly && courses.rows[i].value.visbility == 'private')
                            continue;
                        courses.rows[i].value.href= "#/course/"+courses.rows[i].value._id;
                        returnList.push(courses.rows[i].value);
                    }
                    def.resolve(returnList);
                });
                return def.promise;
            },

            deleteGeneralDoc: function(docid, revid){
                return deleteFromGeneralDb(docid, revid);
            }

        };


        return pouchSkills;


    }]);



})();