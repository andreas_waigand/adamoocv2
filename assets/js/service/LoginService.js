(function () {

    var app = angular.module('login', ["pouch.login",  'ipCookie']);

    app.constant("UserData", {
        name: undefined, //this is technically unnesseccerry but serves to show that this parameter will be used, once its used.
        enrolledIn: {}, //this is technically unnesseccerry but serves to show that this parameter will be used, once its used.
        usertype: undefined, //s.o.
        roles: [],
        loggedIn: false,
        isParticipant: function(){
            return this.usertype && this.usertype == "Participant";
        },
        isDependent: function(){
            return this.usertype && this.usertype == "Dependent";
        },
        isIndependent: function(){
            return this.usertype && this.usertype == "Independent";
        },
        isCollaborative: function(){
            return this.usertype && this.usertype == "Collaborative";
        },
        isCompetitive: function(){
            return this.usertype && this.usertype == "Competitive";
        },
        isAvoidant: function(){
            return this.usertype && this.usertype == "Avoidant";
        }
    });

    //hard coded html Whitelist for people not logged in

    app.constant("WhiteList", {
        "assets/snippets/loginUI.html": true,
        "assets/snippets/landingUI.html": true,
        "assets/snippets/signupUI.html": true,
        "assets/snippets/courseUI.html": true,
        "assets/snippets/singleCourseUI.html": true,
        "assets/snippets/setupUI.html": true
    });

    app.factory('loginService', ['$rootScope', '$q', '$location', 'pouchDBService.login', 'ipCookie', 'UserData', "WhiteList", function ($rootScope, $q, $location, pouch, cookie, userdata, whitelist) {

        function loginToPouch(username, password) {
            return pouch.loginToRemoteDb(username, password);
        }

        function checkOfflineLogin(username) {
            return pouch.checkIfOfflineDbExists(username);
        }

        function loginStateFactory(online, valid) {
            return {
                online: online,
                valid: valid
            };
        }

        function setUserDataForAccess(roles){
            for(var i = 0; i < roles.length; i++){
                if(roles[i]=="_admin"){
                    userdata.createCourses = true;
                    userdata.editCourses = true;
                    userdata.deleteCourses = true;
                    userdata.createSkills = true;
                    userdata.editSkills = true;
                    userdata.deleteSkills = true;
                    userdata.createPlugins = true;
                    userdata.editPlugins = true;
                    userdata.deletePlugins = true;
                }
            }
        }


        function turnOffAllRoles(){
            userdata.createCourses = false;
            userdata.editCourses = false;
            userdata.deleteCourses = false;
            userdata.createSkills = false;
            userdata.editSkills = false;
            userdata.deleteSkills = false;
            userdata.createPlugins = false;
            userdata.editPlugins = false;
            userdata.deletePlugins = false;
        }

        var loginService = {
            username: "",
            loggedIn: false,
            checkedIfLoggedIn: undefined,
            plannedRoute: undefined,

            /**
             * Once this function is called, users who are not logged in can only navigate to sites which are listed in the `WhiteList` constant.
             */

            startListeningToRouteChance: function () {
                var self = this;
                $rootScope.$on("$routeChangeStart", function (event, next, current) {
                    if (!userdata.loggedIn && !whitelist[next.templateUrl]) {
                        self.plannedRoute = $location.path();
                        $location.path("/login");
                    }
                });
            },


            /**
             * This function is used to determine whether or not the current user is currently logged into the CouchDB. If so it sets the necessary userdata and also returns
             a promise resolving to the userdata.
             * @returns {r.promise|promise|.QueueItem.promise|.TaskQueue.promise|TaskQueue.promise|require.QueueItem.promise|*}
             */

            checkIfLoggedIn: function(){
                var self = this;
                if(this.checkedIfLoggedIn)
                   return this.checkedIfLoggedIn.promise;
                self.checkedIfLoggedIn = $q.defer();
                pouch.checkIfLoggedIn().then(function(result){
                    if(result.userdata){
                        setUserDataForAccess(result.userdata.roles);
                        result.name = result.userdata.name;
                        self.checkedIfLoggedIn.resolve(result);
                    }
                    self.checkedIfLoggedIn.resolve(result);
                });
                return self.checkedIfLoggedIn.promise;
            },

            /**
             * This function signs a new user up, creates the user database, logs him in and creates the basic docs in his userdata base.
             The userdata object needs to contain a username, a password and whether or not cookies shall be set for remembering.
             * Signup
             */

            signUp: function(user){
                var def = $q.defer();
                var self = this;
                pouch.signUpToRemoteDb(user).then(function(){
                    self.login(user.username, user.password, user.remember).then(function(result){
                        pouch.createBasicDocs(user).then(function(){
                            def.resolve(result);
                        });
                    })
                }).catch(function(error){
                    def.reject(error);
                });
                return def.promise;
            },

            /**
             * Returns the userinfo document
             * @param username
             * @returns {*}
             */

            getAdditionalUserData: function(username){
              return pouch.getAdditionalUserData(username);
            },



            /**
             * Returns promise which resolves to "offline|online and valid|invalid"
             * @param username
             * @param password
             */
            login: function (username, password, remember) {
                var loginDef = $q.defer();
                var returnVal = loginToPouch(username, password);
                returnVal.then(function (res) {
                    setUserDataForAccess(res.roles);

                    loginDef.resolve(loginStateFactory("online", "valid"));
                    if(remember){
                        document.cookie="username={\"name\":\""+username+"\"};expires=Fri, 3 Aug 2200 20:47:11 UTC;"
                    }else{
                        document.cookie = "username" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    }
                }).catch(function (error) {

                    //Invalid Login
                    if (error.status == "401") {
                        loginDef.resolve(loginStateFactory("online", "invalid"));
                    }
                    //Offline...
                    else {
                        checkOfflineLogin(username).then(function (result) {
                            userdata.usertype = result.usertype;
                            if(remember){
                                document.cookie="username={\"name\":\""+username+"\"};expires=Fri, 3 Aug 2200 20:47:11 UTC;"
                            }else{
                                document.cookie = "username" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                            }
                            loginDef.resolve(loginStateFactory("offline", "valid"));
                        }).catch(function (error) {
                            loginDef.resolve(loginStateFactory("offline", "invalid"));
                        });

                    }
                });

                return loginDef.promise;
            },

            /**
             * This function resets the userdata, deletes a cookie (if set) and ends the couchDb Session. It also returns the user to the login page
             * and reloads the page in order to completly reset all states.
             * @param username
             */

            logout: function(username){
                pouch.logoutOfPouchDb(username);
                if(cookie("username"))
                    document.cookie = "username" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                userdata.loggedIn = false;
                userdata.name = undefined;
                userdata.usertype = undefined;
                userdata.createCourses = false;
                turnOffAllRoles();
                this.loggedIn = false;
                this.checkedIfLoggedIn = undefined;
                $location.path("/login");
                location.reload();
            }


        };

        return loginService;


    }]);
})();
