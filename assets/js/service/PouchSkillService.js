(function(){
    var app = angular.module("pouch.mooc", ["pouch.base", "pouch.startup", "login"]);

    app.constant("pouchDBService.skills.config", {
        skillType: "skill",
        courseType: "course",
        pluginType: "plugin",
        generalDB: "general",
        commonDB: "common",
        highScoreQuery: "highscore/bySkillAndUser",
        skillcollection: "collections/skills",
        coursecollection: "collections/courses",
        plugincollection: "collections/plugins",
        progressListCollection: "hone/allSkills",
        enrolledDocument: "courselist",
        openSkillsDocument:"openlist",
        progressListType: "progresslist"
    });


    app.service("pouchDBService.skills", ['$q','pouch.config','$rootScope', '$http', "pouchDBService.skills.config", "pouchDB", "Plugins", "UserData","userNameConverter", "pouchDBService.plugins" ,function($q,mainconfig, $rootScope, $http, config, pouchDBFactory, pluginData, userData,userNameConverter, plugins){


        var Skills = {};
        var courseSkillRequirementCheckLocks = {};

        function deleteFromGeneralDb(docid, revid){
            var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
            return generalDb.remove(docid,revid);
        }

        function getNotYetOpenSkillsInCourse(course, openlist, skillMap){
            var notYetOpenSkills = [];
            for(var i = 0; i < course.skills.length; i++){
                var curSkill = course.skills[i];
                var curFullSkill = skillMap[curSkill.id];
                curFullSkill.requirements = curSkill.requirements;
                if(!openlist[curFullSkill.responsible] || !openlist[curFullSkill.responsible][curSkill.id]){
                    for(var j = 0; j < curFullSkill.requirements.length; j++){
                        curFullSkill.requirements[j] = skillMap[curFullSkill.requirements[j]];
                    }
                    notYetOpenSkills.push(curFullSkill);
                }
            }
            return notYetOpenSkills;
        }

        function checkIfSingleSkillsRequirementsAreFullfilled(Skill, allSkillsWithProgress){
            for(var i = 0; i < Skill.requirements.length; i++){
                if(!allSkillsWithProgress[Skill.requirements[i]._id] || !allSkillsWithProgress[Skill.requirements[i]._id].done) {
                        return false;
                }
            }
            return true;
        }

        function addOpenSkillsToOpenListDocument(skillsToOpen, openlistdoc){
            var ol = openlistdoc;
                for(var i = 0; i < skillsToOpen.length; i++){
                    openlistdoc[skillsToOpen[i].responsible] = openlistdoc[skillsToOpen[i].responsible] || {};
                    openlistdoc[skillsToOpen[i].responsible][skillsToOpen[i]._id] = {};
                }
            return openlistdoc;
        }

        function getSkillsWhichAreNotYetOpenButWhichRequirementsAreFullfilled(notYetOpenSkills, allSkillsWithProgress){
                var skillsWithFullfilledRequirements = [];
                for(var i = 0; i < notYetOpenSkills.length; i++){
                    if(checkIfSingleSkillsRequirementsAreFullfilled(notYetOpenSkills[i], allSkillsWithProgress)){
                        skillsWithFullfilledRequirements.push(notYetOpenSkills[i]);
                    }
                }
                return skillsWithFullfilledRequirements;
        }


        function checkRequirementsAndUpdateOpenListIfNeccessary(courseDocument, openSkills, allSkills, allSkillProgress) {
            var notYetOpenSkills = getNotYetOpenSkillsInCourse(courseDocument, openSkills, allSkills.skillById);
            if (notYetOpenSkills.length > 0) {
                var shouldBeOpenSkills = getSkillsWhichAreNotYetOpenButWhichRequirementsAreFullfilled(notYetOpenSkills, allSkillProgress);
                if (shouldBeOpenSkills.length > 0)
                    var newOpenList = addOpenSkillsToOpenListDocument(shouldBeOpenSkills, openSkills);
                    return newOpenList;
            }
        }

        function registerToUserDbChanges(user,callback){
            var changes = pouchDBFactory(userNameConverter(user)).changes({
                since: 'now',
                include_docs: true,
                live: true
            }).on('change', function(change) {
                callback(change);
            });
        }

        function registerToGeneralDbChanges(callback){
            var changes = pouchDBFactory(config.generalDB).changes({
                since: 'now',
                include_docs: true,
                live: true
            }).on('change', function(change) {
                callback(change);
            });
        }

        function openLocksCallbackOnProgressListChange(changes){
            if(changes.doc.type && changes.doc.type == config.progressListType){
                courseSkillRequirementCheckLocks = {};
            }
        }

        function openLocksCallbackOnChangedGeneral(changes){
            if(changes.doc.type && changes.doc.type == config.courseType) {
                delete courseSkillRequirementCheckLocks[changes.doc._id];
            }
        }



        var pouchSkills = {

            /**
             * Initializes all variables.
             * This will primarly be called when a user logs in our out.
             */

            init: function(){
                Skills = undefined;
            },

            /**
             * This function gets the highscores for all skills in the given array. The idea
             is that it gets called by the CourseController to tally up the highscores of all
             participants.
             * @param skills
             * @returns {r.promise|promise|.QueueItem.promise|.TaskQueue.promise|TaskQueue.promise|require.QueueItem.promise|*}
             */

            getHighScoreForSkills: function(skills){
                var def = $q.defer();
                var commonDB = pouchDBFactory(config.commonDB);
                var returnList = {};
                var returnArray= [];
                commonDB.query(config.highScoreQuery,{reduce:true}).then(function(result){
                   var skillsScore = result.rows[0].value;
                    for(var i = 0; i < skills.length; i++){
                        if(skillsScore[skills[i]._id]){
                            for(var user in skillsScore[skills[i]._id]){
                                if(returnList[user]){
                                    returnList[user] = returnList[user] + parseInt(skillsScore[skills[i]._id][user]);
                                }else{
                                    returnList[user] = parseInt(skillsScore[skills[i]._id][user]);
                                }
                            }
                        }
                    }
                    for(var user in returnList){
                        returnArray.push({name:user, score:returnList[user]});
                    }
                   def.resolve([returnArray,returnList]);
                });

                return def.promise;

            },


            /**
             * This function is called when a user opens a course page and if something has changed in the userdb since his last visit. This functions checks whether or not new skills should be opened.
             It does so by first getting all nesseccarry documents (openlist of the user, combined progress-lists of the user and all skills).
             Then it checks for all not yet open skills in the course.
             If there are not yet open skills it checks whether or not they should be opened,
             by checking against the progress made by the user.
             If there are skills which are not yet open, but which should be open, it adds those skills to the openlist document.

             The function itself returns a promise which resolves to an object which contains all progress of the user and all open skills, so the course can display them accordingly.
             * @param username
             * @param courseid
             * @returns {r.promise|promise|.QueueItem.promise|.TaskQueue.promise|TaskQueue.promise|require.QueueItem.promise|*}
             */

            updateOpenSkillsForUserInCourse: function(username,courseid){
                if(courseSkillRequirementCheckLocks[courseid])
                    return courseSkillRequirementCheckLocks[courseid].promise;
                courseSkillRequirementCheckLocks[courseid] = $q.defer();
                var self = this;
                var generalDb = pouchDBFactory(config.generalDB);
                var userDb = pouchDBFactory(userNameConverter(username));
                var allSkills = self.getAllSkills(); //parallel Query
                var openSkills = userDb.get(config.openSkillsDocument); //paralell Query
                generalDb.get(courseid).then(function(courseDocument){
                    userDb.query(config.progressListCollection,{
                        reduce: true
                    }).then(function(allSkillsWithProgress){
                        var allSkillProgress = allSkillsWithProgress.rows && allSkillsWithProgress.rows[0] && allSkillsWithProgress.rows[0].value || {};
                        openSkills.then(function(openSkills) {
                            allSkills.then(function(allSkills){
                                var newOpenList = checkRequirementsAndUpdateOpenListIfNeccessary(courseDocument, openSkills, allSkills, allSkillProgress);
                                var resolveValue = {};
                                resolveValue["progress"] = allSkillProgress;
                                if(newOpenList){
                                   //TODO You could add a congratulation message here
                                    resolveValue["open"] = newOpenList;
                                   userDb.put(newOpenList).then(function(){
                                       courseSkillRequirementCheckLocks[courseid].resolve(resolveValue);
                                   });
                                }
                                else{
                                    resolveValue["open"] = openSkills;
                                    courseSkillRequirementCheckLocks[courseid].resolve(resolveValue);
                                }
                            });

                        });
                    });

                });
                return courseSkillRequirementCheckLocks[courseid].promise;;

            },


            /**
             * - `listenToProgressChanges(username)`
             This function registers callbacks on the userdb and generaldb changes feed.
             It is used so start listening to the db changes, so that the locks which
             keep the longer functions from running without reason, get reset when something changes.
             * @param user
             */
            listenToProgressChanges: function(user){
                registerToUserDbChanges(user,openLocksCallbackOnProgressListChange);
                registerToGeneralDbChanges(openLocksCallbackOnChangedGeneral);
            },

            deleteGeneralDoc: function(docid, revid){
                return deleteFromGeneralDb(docid, revid);
            },

            /**
             * This is a function which is only called for the admin list of all skills.
             It directly checks against the online couchdb for all current skills and
             returns a promise for an array of all skills.
             * @returns {r.promise|promise|.QueueItem.promise|.TaskQueue.promise|TaskQueue.promise|require.QueueItem.promise|*}
             */

            getAllSkillsFromServer: function(){
                var def = $q.defer();
                var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
                generalDb.query(config.skillcollection).then(function(skillList){
                    plugins.getAllPlugins().then(function(){
                        var returnList = [];
                        for(var i = 0; i < skillList.rows.length; i++){

                            var responsiblePlugin = pluginData[skillList.rows[i].value.responsible];
                            if(!responsiblePlugin){
                                deleteFromGeneralDb(skillList.rows[i].value._id, skillList.rows[i].value._rev);
                                continue;
                            }
                            if(responsiblePlugin.singleSkill)
                                skillList.rows[i].value.href = responsiblePlugin.skillURL;
                            else{
                                skillList.rows[i].value.href = responsiblePlugin.skillURL + skillList.rows[i].value._id;
                                skillList.rows[i].value.edithref = responsiblePlugin.editURL + skillList.rows[i].value._id;
                            }
                            returnList.push(skillList.rows[i].value);

                        }
                        def.resolve(returnList);
                    });
                });
                return def.promise;
            },

            submitSingleSkill: function(skill){
                    var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
                    var skillDoc = {
                        type: config.skillType,
                        title: skill.title,
                        description: skill.description,
                        responsible: skill.responsible
                    }
                    return generalDb.post(skillDoc);
            },

            getAllSkills: function(){
                var def = $q.defer();
                var generalDb = pouchDBFactory(config.generalDB);
                generalDb.query(config.skillcollection).then(function(skillList){
                    var returnList = {};
                    returnList.raw = skillList.rows;
                    returnList.skillMap = {};
                    returnList.skillStrings = [];
                    returnList.skillById = {};

                    for(var i = 0; i < skillList.rows.length; i++){
                        returnList.skillStrings[i] = skillList.rows[i].value._id + " - " +  skillList.rows[i].value.responsible + " - "  + skillList.rows[i].value.title + " - " + skillList.rows[i].value.description;
                        returnList.skillMap[skillList.rows[i].value._id + " - " +  skillList.rows[i].value.responsible + " - "  + skillList.rows[i].value.title + " - " + skillList.rows[i].value.description] = skillList.rows[i].value;
                        returnList.skillById[skillList.rows[i].value._id] = skillList.rows[i].value;
                    }
                    def.resolve(returnList);
                });
                return def.promise;
            }

        };


        return pouchSkills;


    }]);



})();