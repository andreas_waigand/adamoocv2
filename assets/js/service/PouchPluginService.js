(function(){
    var app = angular.module("pouch.mooc");

    app.constant("Plugins", {});


    app.service("pouchDBService.plugins", ['$q','pouch.config','$rootScope', '$http', "pouchDBService.skills.config", "pouchDB", "Plugins",function($q,mainconfig, $rootScope, $http, config, pouchDBFactory, pluginData){

        var pluginsDone;

        function deleteFromGeneralDb(docid, revid){
            var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
            return generalDb.remove(docid,revid);
        }

        function getAllPlugins(){
            var def = $q.defer();
            var generalDb = pouchDBFactory(config.generalDB);
            generalDb.query(config.plugincollection).then(function(allPlugins){
                for(var i = 0; i < allPlugins.rows.length; i++){
                    pluginData[allPlugins.rows[i].value.name] = allPlugins.rows[i].value;
                    pluginData.set = true;
                }
                def.resolve(pluginData);
            });
            return def.promise;
        }


        var pouchSkills = {


            deleteGeneralDoc: function(docid, revid){
                return deleteFromGeneralDb(docid, revid);
            },

            /**
             * This function returns a promise for a hash map of all plugins,
             * which is basically a copy of the `Plugins` constant.
             * If refresh is true it recalculates the hash. I
             * s only used by the Plugin Create Controller to
             * immediately show the plugin.
             * @param refresh
             * @returns {*}
             */

            getAllPlugins: function(refresh){
                if(refresh){
                    pluginsDone = getAllPlugins();
                }
                pluginsDone = pluginsDone || getAllPlugins();
                return pluginsDone;
            },


            submitSingleSkill: function(skill){
                var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
                var skillDoc = {
                    type: config.skillType,
                    title: skill.title,
                    description: skill.description,
                    responsible: skill.responsible
                }
                return generalDb.post(skillDoc);
            },

            /**
             * This function returns only plugins which have a createURL, which means all but single skill plugins.
             * It is otherwise identical to `getAllPlugins(true)`.
             * @returns {*}
             */
            getAllPluginsWithCreateURLs: function(){
                var def = $q.defer();
                getAllPlugins().then(function(result){
                    var createPlugins = [];
                    for(var plugin in result){
                        if(result[plugin].createURL)
                            createPlugins.push(result[plugin]);
                    }
                    def.resolve(createPlugins);
                });

                return def.promise;
            },

            /**
             * This function returns a promise for all information about a
             * single plugin. It is used by the plugin editor.
             * @param pluginId
             * @returns {*}
             */

            getSinglePlugin: function(pluginId){
              var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
              return generalDb.get(pluginId);
            },

            /**
             * This function writes the passed plugin into the database.
             It either updates an already existing plugin document (if the passed plugin has _id and _ref properties)
             or creates a new plugin document.
             * @param plugin
             * @returns {*}
             */

            submitPlugin: function(plugin){
                var generalDb = pouchDBFactory(mainconfig.host + "/" + config.generalDB);
                var self = this;
                var pluginDoc = {
                    name: plugin.name,
                    type: config.pluginType,
                    description: plugin.description
                }

                if(plugin._id && plugin._rev){
                    pluginDoc._id = plugin._id;
                    pluginDoc._rev = plugin._rev;
                }

                if(plugin.singleSkill){
                    pluginDoc.singleSkill = true;
                    var skillDoc = {
                        title: pluginDoc.name,
                        description: pluginDoc.description,
                        responsible: pluginDoc.name
                    }
                    self.submitSingleSkill(skillDoc);
                }else{
                    pluginDoc.editURL = plugin.editURL;
                    pluginDoc.createURL = plugin.createURL;
                }
                var def = $q.defer();
                pluginDoc.skillURL = plugin.skillURL;
                generalDb.post(pluginDoc).then(function(response){
                    self.getAllPlugins(true);
                    def.resolve(response);
                });
                return def.promise;
            }


        };


        return pouchSkills;


    }]);



})();