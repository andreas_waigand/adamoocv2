(function(){
    var app = angular.module("pouch.login", ["pouch.base", "pouch.startup"]);


    /**
     * This Service's only purpose is to login to a remote Couchdb and
     * get an AuthToken to identify oneself.
     * It adds the AuthToken
     */

    app.constant('pouch.config', {
      host: "http://193.196.7.76:8080/",
      signUpHost: "http://193.196.7.76:8081/"
    });

    app.constant('pouch.templates', [{
        "_id": "_design/hone",
        "views": {
            "allSkills": {
                "map": "function(doc) {\n  if(doc.type==\"progresslist\")\n    emit(doc._id, doc.skills);\n}",
                "reduce": "function(keys, values, rereduce){\n  var allSkills = {};\n  for(var i = 0; i < values.length; i++){\n    for (var attrname in values[i]) { allSkills[attrname] = values[i][attrname]; }\n  }\n  return allSkills;\n}"
            }
        }
        },{

            "_id": "courselist",
            "enrolledIn": []

        },{

            "_id": "openlist"

        }]
    );





    app.service("pouchDBService.login", ["pouch.config", '$q','pouchDB', "pouchDBService", 'pouch.templates','userNameConverter','$http','$location', function(config,  $q, pouchDBFactory, pouch,templates,userNameConverter, $http, $location){


        function createUserInfoDocument(user){
            return {
                "_id": "userinfo",
                "usertype" : user.usertype
            }
        }

        var pouchLogin = {

            loginDef: undefined,

            checkIfLoggedIn: function(){
                if(this.loginDef)
                    return this.loginDef.promise;
                var self = this;
                var remoteDb = pouchDBFactory(config.host + "/_session");
                self.loginDef = $q.defer();
                remoteDb.getSession().then(function(result){
                    self.loginDef.resolve({online: true, userdata: result.userCtx});
                }).catch(function(result) {
                    self.loginDef.resolve({online:false, userdata: undefined});
                });
                return this.loginDef.promise;
            },

            /**
             * @param username
             * @param password
             * @return Promise for login which can reflect three different outcomes.
             * 1. Status: 401 == WRONG Password
             * 2. Status: 200 == Everything is fine
             * 3. Status: Everything Else == Offline, Meteor-crash etc.
             */

            loginToRemoteDb: function (username, password) {
                var remoteDb = pouchDBFactory(config.host + userNameConverter(username));

                var loginDef = $q.defer();
                remoteDb.login(username, password).then(function (res) {
                    var remoteSessionDb = pouchDBFactory(config.host + "/_session");
                    remoteSessionDb.getSession().then(function(result){
                        res.roles = result.userCtx.roles;
                        loginDef.resolve(res);
                    });
                }).catch(function (err) {
                    loginDef.reject(err);
                });
                return loginDef.promise;
            },

            createBasicDocs: function(user){
                var remoteDb = pouchDBFactory(config.host + userNameConverter(user.username));
                var docs = templates;
                docs.push(createUserInfoDocument(user));
                return remoteDb.bulkDocs(docs);
            },

            signUpToRemoteDb: function(user){

                var remoteDb = pouchDBFactory(config.host + userNameConverter(user.username));
                var signUpDef = $q.defer();
                var self = this;
                $http({
                    url: config.signUpHost,
                    method: 'GET',
                    headers: {
                        'Accept': "application/json"
                    },
                    params: {username: user.username, password:user.password}
                }).success(function(data, status, headers, config) {
                    signUpDef.resolve(data);
                }).error(function(data, status, headers, config) {
                    signUpDef.reject(data);
                    });
                return signUpDef.promise;
            },

            /**
             * This function checks if an DB is available in local storage. It is primarly used to check
             * wether or not a Users DB is available in local storage.
             * It is currently checked by performing a query for "decklist", which should exist in every
             * UserDB.
             * TODO: Check against something more general.
             * @param nameOfPossibleOfflineDb
             * @returns {d.promise|promise|fd.g.promise|.n.promise}
             */

            checkIfOfflineDbExists: function (nameOfPossibleOfflineDb) {
                var checkDef = $q.defer();
                var possibleOfflineDb = pouchDBFactory(userNameConverter(nameOfPossibleOfflineDb));
                possibleOfflineDb.get("userinfo").then(function (res) {
                    checkDef.resolve();
                }).catch(function (error) {
                    checkDef.reject();
                    PouchDB.destroy(userNameConverter(nameOfPossibleOfflineDb));
                });

                return checkDef.promise;

            },

            getAdditionalUserData: function(username){
              return pouchDBFactory(userNameConverter(username)).get("userinfo");
            },


            logoutOfPouchDb:function(username){
                var self = this;

                var remoteDb = pouchDBFactory(config.host + userNameConverter(username));
                pouch.stopReplication();
                pouch.init();
                remoteDb.logout(function (err, response) {
                    if (err) {
                        // TODO: network error
                    }
                });
            }


        }
        return pouchLogin;


    }]);



})();
