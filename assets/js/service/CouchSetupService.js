(function(){

    var app = angular.module("couchSetup", ["pouch.login", "pouch.mooc", "pouch.base"]);

    app.constant("couchSetup.generalDesignDocs", {

        "_id": "_design/collections",
        "validate_doc_update": "function (newDoc, oldDoc, userCtx, secObj) { if(userCtx.roles.indexOf('_admin') < 0) { throw({forbidden : \"You have to be admin\"}); } }",
        "views": {
        "skills": {
            "map": "function(doc) {\n  if(doc.type == \"skill\")\n  emit([doc.responsible, doc._id], doc);\n}"
        },
        "courses": {
            "map": "function(doc) {\n  if(doc.type == \"course\")\n  emit(doc._id, doc);\n}"
        },
        "plugins": {
            "map": "function(doc) {\n  if(doc.type == \"plugin\")\n  emit(doc._id, doc);\n}"
        }
        }

    });


    app.constant("couchSetup.commonDesignDocs", {
            "_id": "_design/highscore",
            "views": {
                "bySkillAndUser": {
                    "map": "function(doc) {\n  if(doc.type == 'highscore'){\n    emit(doc.skill,{'user': doc.user, 'score': doc.score});\n  }}",
                    "reduce": "function(keys, values, rereduce){\n  var skills = {};\n  for(var i = 0; i < keys.length; i++){\n    skills[keys[i][0]] = skills[keys[i][0]] || {};\n    skills[keys[i][0]][values[i].user]=values[i].score;\n  }\n  return skills;\n}"
                }
            }}
    );

    app.service("couchSetupService", ["pouch.config", "pouchDBService.skills.config","pouchDB","couchSetup.generalDesignDocs","pouchDBService.login",'$q',"couchSetup.commonDesignDocs", function(config, skillconfig, pouchDBFactory,generalDesignDoc,loginService, $q, commonDesignDocs){


        var setupService = {
            setupCouchDb: function(user){
                var def = $q.defer();
                var generalDb = pouchDBFactory(config.host + "/" + skillconfig.generalDB);
                var commonDb = pouchDBFactory(config.host + "/" + skillconfig.commonDB);
                var cdbpromise = commonDb.put(commonDesignDocs);
                generalDb.put(generalDesignDoc).then(function(){
                    cdbpromise.then(function(){
                        var completeUser = {};
                        completeUser.username = user;
                        completeUser.usertype = "participant";
                        loginService.createBasicDocs(completeUser).then(function(){
                            def.resolve();
                        });
                    });

                });
                return def.promise;
            }
        }
        return setupService;
    }]);


})();