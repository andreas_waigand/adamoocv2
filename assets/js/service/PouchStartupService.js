
(function () {
        var app = angular.module("pouch.startup", ["pouch.base", "pouch.login", "pouch.mooc"]);


    app.service('pouchDBService', ['$q', '$timeout', '$rootScope', 'pouch.config', "pouchDB", "pouchDBService.skills.config", "pouchDBService.skills","pouchDBService.courses","userNameConverter", function ($q, $timeout, $rootScope,  config, pouchDBFactory, skillconfig, skills,courses, userNameConverter) {

        function resetAllFields(self){
            self.mainDb = undefined;
            self.startUp =  $q.defer();
            self.username = "";
            self.remoteSync= undefined;

        }

        var pouchDBService = {

            mainDb: undefined,
            host: config.host + "/",
            //If first startup is done
            startUp: undefined,
            username: "",
            remoteSync:undefined,

            /**
             * This function stops all live syncs and replication which was activated.
             * It should be used when the user logs out.
             */

            stopReplication: function(){
                this.remoteSync && this.remoteSync.cancel();
            },

            init: function(){
            this.startUp = undefined;
            this.username = undefined;
            this.landingUp = undefined;
            },

            landingStartup: function(){
              if(this.landingUp)
                  return this.landingUp.promise;
              this.landingUp =  $q.defer();
              var self = this;
                var remoteGeneralDb = pouchDBFactory(this.host + skillconfig.generalDB);
              var generalDb = pouchDBFactory(skillconfig.generalDB);
              PouchDB.replicate(remoteGeneralDb,generalDb).then(function(res){
                 self.landingUp.resolve();
              }).catch(function(res){
                  self.landingUp.resolve();
              });
              return self.landingUp.promise;
            },

            /**
             * Prepares all neccessary data and services on startup
             * It returns a promise which gets resolves when the startup is successful.
             * @param online True if user is online, false if offline
             * @param username Username of current user
             * @returns {d.promise|promise|fd.g.promise|.n.promise}
             */

            generalStartup: function (online, username) {
                var self = this;
                if(self.startUp)
                    return self.startUp.promise;

                resetAllFields(this);
                self.startUp = $q.defer();
                self.username = username;
                self.mainDb = pouchDBFactory(userNameConverter(self.username));
                if (online) {
                    self.onlineStartup().then(function (result) {
                        self.offlineStartup().then(function (res) {
                            self.startUp.resolve();
                        });
                    }).catch(function (error) {

                    });
                } else {
                    self.offlineStartup().then(function (res) {
                        self.startUp.resolve();
                    });
                }

                return self.startUp.promise;
            },


            offlineStartup: function () {
                var offDef = $q.defer();
                skills.listenToProgressChanges(this.username);
                offDef.resolve();
                return offDef.promise;;
            },

            startSyncOfUserDb: function () {
                var syncDef = $q.defer();
                var self = this;
                var remoteDb = pouchDBFactory(self.host + userNameConverter(self.username));

                var repl = PouchDB.sync(remoteDb, self.mainDb);
                repl.on('complete', function (res) {
                    self.remoteSync = PouchDB.sync(remoteDb,self.mainDb,{live: true});
                    courses.updateEnrolledStatusInUserData().then(function(result){
                        syncDef.resolve(res);
                    });
                }).on('error', function (err) {
                    //Hack for PouchDB Issue #135
                    syncDef.resolve(err);
                });

                return syncDef.promise;
            },


            onlineStartup: function () {
                var startDef = $q.defer();
                var self = this;
                var remoteSkillDb = pouchDBFactory(self.host + skillconfig.generalDB);
                var remoteCommonDb = pouchDBFactory(self.host + skillconfig.commonDB);
                var commonDb = pouchDBFactory(skillconfig.commonDB);
                var skillDb = pouchDBFactory(skillconfig.generalDB);
                var repSkills = remoteSkillDb.replicate.to(skillDb);
                var repCommon = remoteCommonDb.replicate.to(commonDb);
                self.startSyncOfUserDb().then(function (result) {
                        repSkills.then(function(){
                            skills.init();
                            PouchDB.replicate(remoteSkillDb, skillDb, {live: true});
                            repCommon.then(function(){
                                PouchDB.replicate(remoteCommonDb,commonDb, {live:true});
                                startDef.resolve();
                            });
                        });
                }).catch(function (error) {
                    //For some reason replication failed, so I wont try replicating the decks.
                    //Just retry regularly and go for offline for now
                    startDef.resolve();
                });

                return startDef.promise;
            }

        };


        return pouchDBService;
    }]);

})();




