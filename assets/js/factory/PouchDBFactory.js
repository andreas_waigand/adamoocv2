/**
 * This service mainly exists in order to make testing easier.
 * It was only a small change so I think it is acceptable
 */

(function(){

    var app = angular.module("pouch.base", []);

    app.factory("userNameConverter", function userNameConverterFactory() {
        function stringEncode(string){
            var hex, i;

            var result = "";
            for (i=0; i<string.length; i++) {
                hex = string.charCodeAt(i).toString(16);
                result += ("000"+hex).slice(-4);
            }

            return result;
        }

        return function(userName){
            return "userdb-" + stringEncode(userName);
        }
    });

    app.factory("pouchDB", function pouchDBFactory() {

        return function (dbName){
            return new PouchDB(dbName);
        };

    });


})();